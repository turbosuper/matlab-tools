clear
home
close all

%load the values
file_6ac_12 = 'sin_cos_values_6ac_scaled_2to12.txt';
file_6ac_14 = 'sin_cos_values_6ac_scaled_2to14.txt';
file_8ac_16 = 'sin_cos_values_8ac_scaled_2to16.txt';
file_8ac_18 = 'sin_cos_values_8ac_scaled_2to18.txt';

delimiterIn = ' ';
A_6ac_12 = importdata(file_6ac_12,delimiterIn);
A_6ac_14 = importdata(file_6ac_14,delimiterIn);
A_8ac_16 = importdata(file_8ac_16,delimiterIn);
A_8ac_18 = importdata(file_8ac_18,delimiterIn);
sin_6ac_12 = A_6ac_12(:,1)';
cos_6ac_12 = A_6ac_12(:,2)';
sin_6ac_14 = A_6ac_14(:,1)';
cos_6ac_14 = A_6ac_14(:,2)';
sin_8ac_16 = A_8ac_16(:,1)';
cos_8ac_16 = A_8ac_16(:,2)';    
sin_8ac_18 = A_8ac_18(:,1)';
cos_8ac_18 = A_8ac_18(:,2)';

%scale down the vectors
sin_6ac_12 = sin_6ac_12 / 2^11;
cos_6ac_12 = cos_6ac_12 / 2^11;
sin_6ac_14 = sin_6ac_14 / 2^13;
cos_6ac_14 = cos_6ac_14 / 2^13;
sin_8ac_16 = sin_8ac_16 / 2^15;
cos_8ac_16 = cos_8ac_16 / 2^15;
sin_8ac_18 = sin_8ac_18 / 2^17;
cos_8ac_18 = cos_8ac_18 / 2^17;

%generate ideal values
x = 0 : 0.01: 37.6991;
sin_x = sin(x);
cos_x = cos(x);

%calculate the error
sin_6ac_12_error = abs(sin_6ac_12 - sin_x);
sin_6ac_14_error = abs(sin_6ac_14 - sin_x);
sin_8ac_16_error = abs(sin_8ac_16 - sin_x);
sin_8ac_18_error = abs(sin_8ac_18 - sin_x);
cos_6ac_12_error = abs(cos_6ac_12 - cos_x);
cos_6ac_14_error = abs(cos_6ac_14 - cos_x);
cos_8ac_16_error = abs(cos_8ac_16 - cos_x);
cos_8ac_18_error = abs(cos_8ac_18 - cos_x);

%use one period for the calculations
x_error_ref = x(1:316);
sin_6ac_12_error_ref = sin_6ac_12_error(1:316);
sin_6ac_14_error_ref = sin_6ac_14_error(1:316);
sin_8ac_16_error_ref = sin_8ac_16_error(1:316);
sin_8ac_18_error_ref = sin_8ac_18_error(1:316);
cos_6ac_12_error_ref = cos_6ac_12_error(1:316);
cos_6ac_14_error_ref = cos_6ac_14_error(1:316);
cos_8ac_16_error_ref = cos_8ac_16_error(1:316);
cos_8ac_18_error_ref = cos_8ac_18_error(1:316);

%perform the FFT on the data
Fs = 1000; %Sampling Frequency
T = 1/Fs;
L = 3770;
t = x;
NFFT  = 2^nextpow2(L);
f = Fs/2*linspace(0,1,NFFT/2+1);
Y_sin_6_12 = (fft(sin_6ac_12,NFFT)/L);
Y_sin_6_14 = (fft(sin_6ac_12,NFFT)/L);
Y_sin_8_16 = (fft(sin_6ac_12,NFFT)/L);
Y_sin_8_18 = (fft(sin_6ac_12,NFFT)/L);
Y_cos_6_12 = (fft(cos_6ac_12,NFFT)/L);
Y_cos_6_14 = (fft(cos_6ac_12,NFFT)/L);
Y_cos_8_16 = (fft(cos_6ac_12,NFFT)/L);
Y_cos_8_18 = (fft(cos_6ac_12,NFFT)/L);


%%%% VISUALISE EVERYTHING
figure(1)
subplot(2,4,1)
plot(x,sin_x,'.') %, 'MarkerSize',10)
hold
plot(x,sin_6ac_12,'.') %, 'MarkerSize',10)
title('12 bit sine values comparisson')
xlabel('0 < x < 12pi') 
ylabel('sin') 
grid on
legend({'Reference','Scaled'},'Location','southwest')

subplot(2,4,2)
plot(x,sin_x,'.') %, 'MarkerSize',10)
hold
plot(x,sin_6ac_14,'.') %, 'MarkerSize',10)
title('14 bit sine values comparisson')
xlabel('0 < x < 12pi') 
ylabel('sin') 
grid on

subplot(2,4,3)
plot(x,sin_x,'.') %, 'MarkerSize',10)
hold
plot(x,sin_8ac_16,'.') %, 'MarkerSize',10)
title('14 bit sine values comparisson')
xlabel('0 < x < 12pi') 
ylabel('sin') 
grid on

subplot(2,4,4)
plot(x,sin_x,'.') %, 'MarkerSize',10)
hold
plot(x,sin_8ac_18,'.') %, 'MarkerSize',10)
title('18 bit sine values comparisson')
xlabel('0 < x < 12pi') 
ylabel('sin') 
grid on

subplot(2,4,5)
plot(x,cos_x,'.') %, 'MarkerSize',10)
hold
plot(x,cos_6ac_12,'.') %, 'MarkerSize',10)
title('12 bit cosine values comparisson')
xlabel('0 < x < 12pi') 
ylabel('cosine') 
grid on
legend({'Reference','Scaled'},'Location','southwest')

subplot(2,4,6)
plot(x,cos_x,'.') %, 'MarkerSize',10)
hold
plot(x,cos_6ac_14,'.') %, 'MarkerSize',10)
title('14 bit cosine values comparisson')
xlabel('0 < x < 12pi') 
ylabel('cosine') 
grid on

subplot(2,4,7)
plot(x,cos_x,'.') %, 'MarkerSize',10)
hold
plot(x,cos_8ac_16,'.')%, 'MarkerSize',10)
title('16 bit cosine values comparisson')
xlabel('0 < x < 12pi') 
ylabel('cosine') 
grid on

subplot(2,4,8)
plot(x,cos_x,'.')%, 'MarkerSize',10)
hold
plot(x,cos_8ac_18,'.') %, 'MarkerSize',10)
title('18 bit cosine values comparisson')
xlabel('0 < x < 12pi') 
ylabel('cosine') 
grid on

figure(2)
subplot(2,4,1)
plot(x_error_ref, sin_6ac_12_error_ref,'.')
title('12 bit sine error')
xlabel('0 < x < pi') 
ylabel('error magnitude') 
grid on

subplot(2,4,2)
plot(x_error_ref, sin_6ac_14_error_ref,'.')
title('14 bit sine error')
ylabel('error magnitude') 
xlabel('0 < x < pi') 
grid on

subplot(2,4,3)
plot(x_error_ref, sin_8ac_16_error_ref,'.')
title('16 bit sine error')
ylabel('error magnitude') 
xlabel('0 < x < pi') 
grid on

subplot(2,4,4)
plot(x_error_ref, sin_8ac_18_error_ref,'.')
title('18 bit sine error')
ylabel('error magnitude') 
xlabel('0 < x < pi') 
grid on

subplot(2,4,5)
plot(x_error_ref, cos_6ac_12_error_ref,'.')
title('12 bit cosine error')
ylabel('error magnitude') 
xlabel('0 < x < pi') 
grid on

subplot(2,4,6)
plot(x_error_ref, cos_6ac_14_error_ref,'.')
title('14 bit cosine error')
ylabel('error magnitude') 
xlabel('0 < x < pi') 
grid on

subplot(2,4,7)
plot(x_error_ref, cos_8ac_16_error_ref,'.')
title('16 bit cosine error')
ylabel('error magnitude') 
xlabel('0 < x < pi') 
grid on

subplot(2,4,8)
plot(x_error_ref, cos_8ac_18_error_ref,'.')
title('18 bit cosine error')
ylabel('error magnitude') 
xlabel('0 < x < pi') 
grid on

figure(3)
subplot(2,1,1)
plot(x_error_ref, sin_6ac_12_error_ref,'.',x_error_ref, sin_6ac_14_error_ref,'.',x_error_ref, sin_8ac_16_error_ref,'.',x_error_ref, sin_8ac_18_error_ref,'.')
title('Sine error comparison')
ylabel('error magnitude') 
xlabel('0 < x < pi') 
legend({'12bit','14bit','16bit','18bit'},'Location','southwest')
grid on

subplot(2,1,2)
plot(x_error_ref, cos_6ac_12_error_ref,'.',x_error_ref, cos_6ac_14_error_ref,'.',x_error_ref, cos_8ac_16_error_ref,'.',x_error_ref, cos_8ac_18_error_ref,'.')
title('Cosine error comparison')
ylabel('error magnitude') 
xlabel('0 < x < pi') 
legend({'12bit','14bit','16bit','18bit'},'Location','southwest')
grid on

figure(4)
subplot(2,4,1)
plot(f,abs(Y_sin_6_12(1:NFFT/2+1)));
title('12 bit sine error amplitude spectrum')

subplot(2,4,2)
plot(f,abs(Y_sin_6_14(1:NFFT/2+1)));
title('14 bit sine error amplitude spectrum')


subplot(2,4,3)
plot(f,abs(Y_sin_8_16(1:NFFT/2+1)));
title('16 bit sine error amplitude spectrum')

subplot(2,4,4)
plot(f,abs(Y_sin_8_18(1:NFFT/2+1)));
title('12 bit sine error amplitude spectrum')

subplot(2,4,5)
plot(f,abs(Y_cos_6_12(1:NFFT/2+1)));
title('12 bit cosine error amplitude spectrum')

subplot(2,4,6)
plot(f,abs(Y_cos_6_14(1:NFFT/2+1)));
title('14 bit cosine error amplitude spectrum')

subplot(2,4,7)
plot(f,abs(Y_cos_8_16(1:NFFT/2+1)));
title('16 bit cosine error amplitude spectrum')

subplot(2,4,8)
plot(f,abs(Y_cos_8_18(1:NFFT/2+1)));
title('18 bit cosine error amplitude spectrum')





%%%%%%NOCMAJL
figure(5)
subplot(2,2,1)
plot(x,sin_x,'.') %, 'MarkerSize',10)
hold
plot(x,sin_6ac_12,'.') %, 'MarkerSize',10)
title('12 bit sine values comparisson')
xlabel('0 < x < 12pi') 
ylabel('sin') 
grid on
legend({'Reference','Scaled'},'Location','southwest')

subplot(2,2,2)
plot(x,sin_x,'.') %, 'MarkerSize',10)
hold
plot(x,sin_8ac_18,'.') %, 'MarkerSize',10)
title('18 bit sine values comparisson')
xlabel('0 < x < 12pi') 
ylabel('sin') 
grid on

subplot(2,2,3)
plot(x,cos_x,'.') %, 'MarkerSize',10)
hold
plot(x,cos_6ac_12,'.') %, 'MarkerSize',10)
title('12 bit cosine values comparisson')
xlabel('0 < x < 12pi') 
ylabel('cosine') 
grid on
legend({'Reference','Scaled'},'Location','southwest')

subplot(2,2,4)
plot(x,cos_x,'.')%, 'MarkerSize',10)
hold
plot(x,cos_8ac_18,'.') %, 'MarkerSize',10)
title('18 bit cosine values comparisson')
xlabel('0 < x < 12pi') 
ylabel('cosine') 
grid on

figure(6)
subplot(2,2,1)
plot(x_error_ref, sin_6ac_12_error_ref,'.')
title('12 bit sine error')
xlabel('0 < x < pi') 
ylabel('error magnitude') 
grid on

subplot(2,2,2)
plot(x_error_ref, sin_8ac_18_error_ref,'.')
title('18 bit sine error')
ylabel('error magnitude') 
xlabel('0 < x < pi') 
grid on

subplot(2,2,3)
plot(x_error_ref, cos_6ac_12_error_ref,'.')
title('12 bit cosine error')
ylabel('error magnitude') 
xlabel('0 < x < pi') 
grid on

subplot(2,2,4)
plot(x_error_ref, cos_8ac_18_error_ref,'.')
title('18 bit cosine error')
ylabel('error magnitude') 
xlabel('0 < x < pi') 
grid on

figure(7)
subplot(2,1,1)
plot(x_error_ref, sin_6ac_12_error_ref,'.',x_error_ref, sin_8ac_18_error_ref,'.')
title('Sine error comparison')
ylabel('error magnitude') 
xlabel('0 < x < pi') 
legend({'12bit','18bit'},'Location','southwest')
grid on

subplot(2,1,2)
plot(x_error_ref, cos_6ac_12_error_ref,'.',x_error_ref, cos_8ac_18_error_ref,'.')
title('Cosine error comparison')
ylabel('error magnitude') 
xlabel('0 < x < pi') 
legend({'12bit','18bit'},'Location','southwest')
grid on

figure(8)
subplot(2,2,1)
plot(f,abs(Y_sin_6_12(1:NFFT/2+1)));
title('12 bit sine error amplitude spectrum')

subplot(2,2,2)
plot(f,abs(Y_sin_8_18(1:NFFT/2+1)));
title('18 bit sine error amplitude spectrum')

subplot(2,2,3)
plot(f,abs(Y_cos_6_12(1:NFFT/2+1)));
title('12 bit cosine error amplitude spectrum')

subplot(2,2,4)
plot(f,abs(Y_cos_8_18(1:NFFT/2+1)));
title('18 bit cosine error amplitude spectrum')
