clear
home
close all

 
%fid = fopen('sin_cos_values.txt', 'wt')

%create sin and cos
x = 0 : 0.01: 25.1327;
sin_x = sin(x);
cos_x = cos(x);

quanta_bits = 24

sin_x = (sin_x * 2^(quanta_bits-1));
cos_x = (cos_x * 2^(quanta_bits-1));

q_sin_x = round(sin_x);% * 2^(quanta_bits-1));
q_cos_x = round(cos_x);% * 2^(quanta_bits-1));

sin_error_vector = sin_x - q_sin_x;

t_atan2 = rad2deg(atan2(sin_x,cos_x));
q_atan2 = rad2deg(atan2(q_sin_x,q_cos_x));

atan_error_vector = (t_atan2) - (q_atan2);

%---------------atan error------------------
%calculate the atan error maxima
[atan_max_error_value, atan_max_error_index] = max(abs(atan_error_vector));
atan_max_error_value
atan_angle_max_error = x(atan_max_error_index)

%calculate the atan relative error
atan_relative_error = abs(q_atan2 - t_atan2) ./ t_atan2;
[max_atan_relative_error_value, max_atan_relative_error_index] = max(abs(atan_relative_error));
max_atan_relative_error_value
atan_angle_max_relative_error = x(max_atan_relative_error_index)

%calculte the atan standard deviation
atan_error_values_count = length(atan_error_vector); 
atan_error_std_dev = sum((atan_error_vector).^2);
atan_error_std_dev = sqrt((atan_error_std_dev/(atan_error_values_count -1)));
atan_error_std_dev

%------------sin error---------------
%calculate the sin error maxima
[sin_max_error_value, sin_max_error_index] = max(abs(sin_error_vector));
sin_max_error_value
sin_angle_max_error = x(sin_max_error_index)

%calculate the atan relative error
sin_relative_error = abs(sin_x - q_sin_x) ./ sin_x;
[max_sin_relative_error_value, max_sin_relative_error_index] = max(abs(sin_relative_error));
max_sin_relative_error_value
angle_max_sin_relative_error =x(max_sin_relative_error_index)

%calculte the atan standard deviation
sin_error_values_count = length(sin_error_vector); 
sin_error_std_dev = sum((sin_error_vector).^2);
sin_error_std_dev = sqrt((sin_error_std_dev/(sin_error_values_count -1)));
sin_error_std_dev

figure(1)
subplot(2,1,1)
plot(x,sin_x,'g',x,q_sin_x,'--','Markersize',10)
hold
plot(x,sin_error_vector,'r')
grid on
subplot(2,1,2)
plot(x,t_atan2,'g',x,q_atan2,'--','Markersize',10)
hold
plot(x,atan_error_vector,'r')