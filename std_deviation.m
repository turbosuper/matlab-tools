clear
home
close all

%create the reference values
x = 0 : 0.1: 12.5664;
mlab_sin_x = sin(x);
mlab_cos_x = cos(x);


%load the values
filename = 'matlab_input_values_6ac_2to12_scaled.txt'
power_of_2 = 12;
delimiterIn = ' ';
A = importdata(filename,delimiterIn);
python_sin_values = A(:,1)';
python_cos_values = A(:,2)';

%scale the vectors down
python_sin_values = python_sin_values / 2^(power_of_2-1); %11 - the power of 12 Bit variable
python_cos_values = python_cos_values / 2^(power_of_2-1);

%calcute the difference between the measured and the ideal value
sin_error_per_value = python_sin_values - mlab_sin_x;
cos_error_per_value = python_cos_values - mlab_cos_x;

%show the maximal error and where it is
abs_sin_error_per_value = abs(sin_error_per_value);
[sinMaxError, sinMaxErrorI] = max(abs_sin_error_per_value);
sinMaxError
sinMaxErrorArg = x(sinMaxErrorI)
abs_cos_error_per_value = abs(cos_error_per_value);
[cosMaxError, cosMaxErrorI] = max(abs_cos_error_per_value);
cosMaxError
cosMaxErrorArg = x(cosMaxErrorI)

%calculate the standard deviation
sin_values_count = length(sin_error_per_value); 
sin_std_dev = sum((sin_error_per_value).^2);
sin_std_dev = sqrt((1/(sin_values_count -1)*sin_std_dev));

cos_values_count = length(cos_error_per_value); 
cos_std_dev = sum((cos_error_per_value).^2);
cos_std_dev = sqrt((1/(cos_values_count -1)*cos_std_dev));

sin_std_dev
cos_std_dev
