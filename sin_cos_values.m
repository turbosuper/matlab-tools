clear
home
close all
 
fid = fopen('matlab_input_values.txt', 'wt')
fid2 = fopen('matlab_output_values.txt', 'wt')
%fid3 = fopen('matlab_test_columns.txt', 'wt')

x = 0 : 0.1: 12.5664;
sin_x = sin(x);
cos_x = cos(x);

%save this values as input
A = [sin_x;cos_x];
B = A.';
%C = 0 : 1 : 67;
fprintf(fid, '%.6f %.6f \n', A);

%fprintf(fid3, '%d %d \n', C);

%create a vector with angle difference between the values
arc_t = atan(sin_x ./ cos_x);
%save the values
fprintf(fid2, '%2.10f \n', arc_t);

%show sine and cosine wave
figure(1)
plot(x,sin_x,'.', 'MarkerSize',14)
hold
plot(x,cos_x,'.', 'MarkerSize',14)
title('Sinus und Cosinus Werte')
xlabel('0 < x < 4pi') 
ylabel('Sine and Cosine Values') 
grid on
legend({'y = sin(x)','z = cos(x)'},'Location','southwest')

%show angle 
figure(2)
%subplot(2,1,2)
plot(x,arc_t, '.', 'MarkerSize',13)
grid on
xlabel('Winkel 0 < x < 3pi') 
ylabel('Grad: -90 bis 270 [�]') 
title('Winkel als atan(sin/cos)')

%show all together
figure(3)
plot(x,sin_x, 'Linewidth',3)
hold
plot(x,cos_x, 'Linewidth',3)
plot(x,arc_t,'Linewidth',3)
title('Sinus und Cosinus mit Winkel')
xlabel('0 < x < 4pi')  
legend({'sin(x)','cos(x)','winkel'},'Location','southwest')
grid on

fclose(fid)
fclose(fid2)
%fclose(fid3)