clear
home
close all
 
fid = fopen('sin_cos_values.txt', 'wt')

x = 0 : 0.01: 37.6991;
sin_x = sin(x);
cos_x = cos(x);

%save this values as input
A = [sin_x;cos_x];
B = A.';
%C = 0 : 1 : 67;
fprintf(fid, '%.8f %.8f \n', A);

figure(1)
plot(x,sin_x,'.', 'MarkerSize',12)
hold
plot(x,cos_x,'.', 'MarkerSize',12)
title('Sinus und Cosinus Werte')
xlabel('0 < x < 12pi') 
ylabel('Sine and Cosine Values') 
grid on
legend({'y = sin(x)','z = cos(x)'},'Location','southwest')


fclose(fid)

%fclose(fid3)