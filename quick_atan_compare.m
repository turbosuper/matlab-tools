clear
home
close all

%load the data
%The data from modul output file need to be scaled to the reference values:
%the min/max of atan2 with matlab, show the exact values for scaling.
f_modul_in = 'module_input_vector.txt'
f_modul_out = 'module_output_vector_scaled.txt'
delimiterIn = ' ';
input_matrix = importdata(f_modul_in,delimiterIn);
output_matrix = importdata(f_modul_out,delimiterIn);

%save to vectors
sin_input = input_matrix(1:end-1,1);
cos_input = input_matrix(1:end-1,2);
modul_atan = output_matrix(2:end,1);

%calculate atan from the matlab generated vectors
%atan2 to consider quadrants
matlab_atan = rad2deg(atan2(sin_input,cos_input));
min_matlab_atan = min(matlab_atan)
max_matlab_atan = max(matlab_atan)


%t must be 1 shorter for the correction
t = (0: 0.01 : 12.5664-0.01);

%show the errors
error_vector = (matlab_atan - modul_atan);
        

figure(1)
subplot(2,2,1)
plot(t, matlab_atan, '.', 'MarkerSize',12)
title('Matlab Berechnung')
xlabel('0 < x < 4pi') 
ylabel('Winkel -180 bis +180') 
grid on
subplot(2,2,2)
plot(t, modul_atan, '.', 'MarkerSize',12)
title('Modul Berechnung')
xlabel('0 < x < 4pi') 
ylabel('Winkel -180 bis +180') 
grid on
subplot(2,2,3)
plot(t, matlab_atan, '.', t, modul_atan, '.')
title('Vergleich')
xlabel('0 < x < 4pi') 
ylabel('Winkel -180 bis +180') 
legend({' Matlab','Modul'},'Location','northeast')
grid on
subplot(2,2,4)
plot(t, error_vector, '.')
title('Fehler')
xlabel('0 < x < 4pi') 
ylabel('Winkel -180 bis +180') 
grid on
