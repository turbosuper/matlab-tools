close all
home
clear all

bit_breite12_std_abw = 0.007;
bit_breite14_std_abw = 0.0064;
bit_breite16_std_abw = 0.0064;
bit_breite18_std_abw = 0.0064;

x = 10 : 2 : 20;

breite12_durchlauf_4 = 55.7491;
breite12_durchlauf_8 = 0.2571;
breite12_durchlauf_14 = 0.0078;
breite12_durchlauf_16 = 0.007;
breite12_durchlauf_24 = 0.0068;
breite12_durchlauf_32 = 0.0068;

breite14_durchlauf_4 = 55.7492;
breite14_durchlauf_8 = 0.2572;
breite14_durchlauf_14 = 0.0074;
breite14_durchlauf_16 = 0.0064;
breite14_durchlauf_24 = 0.0064;
breite14_durchlauf_32 = 0.0064;

t = [8, 14, 16, 24, 32];
b12_durchl = [0.2571, 0.0078, 0.007, 0.0068, 0.0068];
b14_durchl = [0.2572, 0.0074, 0.0064, 0.0064, 0.0064];

t = t(2:end)
b12_durchl = b12_durchl(2:end)
b14_durchl = b14_durchl(2:end)


figure(1)
plot(12, bit_breite12_std_abw, '*', 14, bit_breite14_std_abw, '*', 16, bit_breite16_std_abw, '*', 18, bit_breite18_std_abw, '*')
legend('12 Bit breit','14 Bit Breit', '16 Bit breit', '18 Bit breit')
axis([10 20 0.005 0.008])
ylabel('Standard Abweichung [Grad]');
xlabel('Eingangsvektor Breite [Bit]');
grid on;

figure(2)
plot(t, b12_durchl,'-mo','MarkerEdgeColor','m','MarkerFaceColor','m')
hold
plot(t, b14_durchl,'-mo','MarkerEdgeColor','g','MarkerFaceColor','g') 
legend('12 Bit breit','14 Bit breit')
axis([12 34 0.006 0.008])
ylabel('Standard Abweichung [Grad]');
xlabel('Durchläufe des Cordic Alogrithums');
grid on;