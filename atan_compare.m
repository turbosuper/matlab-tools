clear
home
close all

%load the data
reference_data_in = 'sin_cos_values.txt'
module_data_out = 'modul_output_values.txt'
delimiterIn = ' ';
reference_matrix = importdata(reference_data_in,delimiterIn);
module_output = importdata(module_data_out,delimiterIn);
bit_width = 12;
iterations = 16;

%save to vectors
sin_input = reference_matrix(1:end-1,1);
cos_input = reference_matrix(1:end-1,2);
module_atan = module_output(2:end,1);

%The data from modul output file needs to be scaled to the reference values:
%the min/max of atan2 with matlab, show the exact values for scaling.

%calculate atan from the matlab generated vectors
%atan2 to consider quadrants
matlab_atan = rad2deg(atan2(sin_input,cos_input));
min_matlab_atan = min(matlab_atan);
max_matlab_atan = max(matlab_atan);


%t must be 1 shorter for the correction
t = (0: 0.01 : 25.1327-0.01);

%calculate the error
error_vector = (matlab_atan) - (module_atan);

%calculate the error maxima
[max_error_value, max_error_index] = max(error_vector);
max_error_value
angle_max_error = module_atan(max_error_index)

%calculate the relative error
relative_error = (module_atan - matlab_atan)./matlab_atan;
[max_relative_error_value, max_relative_error_index] = max(relative_error);
max_relative_error_value
angle_max_relative_error = module_atan(max_relative_error_index)

%calculte the standard deviation
error_values_count = length(error_vector); 
error_std_dev = sum((error_vector).^2);
error_std_dev = sqrt((1/(error_values_count -1)*error_std_dev));
error_std_dev

figure(1)
subplot(2,2,1)
plot(t, matlab_atan, '.', 'MarkerSize',12)
title('Matlab Berechnung')
xlabel('0 < x < 4pi') 
ylabel('Winkel -180 bis +180') 
grid on
subplot(2,2,2)
plot(t, module_atan, '.', 'MarkerSize',12)
title("Modul Berechnung ("+ bit_width + " bitbreit Eingangsvektor, " + iterations + " Durchlaufe)")
xlabel('0 < x < 4pi') 
ylabel('Winkel -180 bis +180') 
grid on
% subplot(2,2,3)
% plot(t, matlab_atan, '.', t, modul_atan, '.')
% title('Vergleich')
% xlabel('0 < x < 4pi') 
% ylabel('Winkel -180 bis +180') 
% legend({' Matlab','Modul'},'Location','northeast')
% grid on
subplot(2,2,3:4)
plot(t, error_vector, '.')
title("Fehler bei " +bit_width + " bitbreit Eingangsvektor, " +iterations + " Durchlaufe")
xlabel('0 < x < 4pi') 
ylabel('Winkel -180 bis +180') 
grid on
